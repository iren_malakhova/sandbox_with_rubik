#!/usr/bin/python 2.7
#-*- coding: utf-8 -*-
from sys import argv
 
x = int(argv[1])


def make_side(value):
    global x
    side = []
    for i in range(x):
        l = []
        for  j in range(x):
          l.append(value)
        side.append(l)
    return side

def make_cube():
    # cube[0] -> up
    # cube[1] -> back
    # cube[2] -> left
    # cube[3] -> front
    # cube[4] -> right
    # cube[5] -> down
    
    colors = {1:'wite', 2:'red', 3:'blue', 4:'orange', 5:'green', 6:'yellow'}
    cube = []

    for key in colors:
          cube.append(make_side(key))
    return cube
    
def show_cube():
    global cube, x
    print ' ' * 20, '**up**'
    for i in cube[0]:
        print ' ' * 19, i
    print ' ','*back*', ' ' * 2, '*left*', ' ', '*front*', ' ','*right*'
    for e in xrange(x):
        print cube[1][e], cube[2][e], cube[3][e], cube[4][e]
    print ' ' * 20, '*down*'
    for i in cube[5]:
        print ' ' * 19, i
        
def check_color(cube):
    '''
    function checks whether all sides have single color each
    
    check_color([[[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[2, 2, 2], [2, 2, 2], [2, 2, 2]], [[3, 3, 3], [3, 3, 3], [3, 3, 3]], [[4, 4, 4], [4, 4, 4], [4, 4, 4]], [[5, 5, 5], [5, 5, 5], [5, 5, 5]], [[6, 6, 6], [6, 6, 6], [6, 6, 6]]])
    True
    check_color([[[1, 1, 2], [1, 1, 1], [1, 1, 1]], [[2, 2, 1], [2, 2, 2], [2, 2, 2]], [[3, 3, 3], [3, 3, 3], [3, 3, 3]], [[4, 4, 4], [4, 4, 4], [4, 4, 4]], [[5, 5, 5], [5, 5, 5], [5, 5, 5]], [[6, 6, 6], [6, 6, 6], [6, 6, 6]]])
    False
    check_color([[[1, 1, 6], [1, 1, 1], [1, 1, 1]], [[2, 2, 2], [2, 2, 2], [2, 2, 2]], [[3, 3, 3], [3, 3, 3], [3, 3, 3]], [[4, 4, 4], [4, 4, 4], [4, 4, 4]], [[5, 5, 5], [5, 5, 5], [5, 5, 5]], [[6, 6, 6], [6, 6, 6], [6, 6, 1]]])
    False
    '''
    cube = cube
    for sides in cube:
        for x in sides:
            for i in xrange(len(x)):
                j = i + 1
                if j < len(sides):
                  if x[j] != x[i]:
                      return False
    return True
    
def cons_check(cube):
    global x
    check_dict = {1:0, 2:0, 3:0, 4:0, 5:0, 6:0}
    cube = cube
    lenth = x**2
    for sides in cube:
        for y in sides:
            for i in xrange(len(y)):
                k = y[i]
                check_dict[k] += 1
    if check_dict:
        for key in check_dict:
            if check_dict[key] != lenth:
                return False
            return True
    else:
        return False
        
def other_side_calculation(side, segment):
    if segment  < 3:
        if side == 1:
            f_side = 4
            s_side = 3
            t_side = 2
        elif side == 2:
            f_side = 1
            s_side = 4
            t_side = 3
        elif side == 3:
            f_side = 2
            s_side = 1
            t_side = 4
        elif side == 4:
            f_side = 3
            s_side = 2
            t_side = 1
        elif side == 5:
            f_side = 2
            s_side = 0
            t_side = 4
        elif side == 0:
            f_side = 2
            s_side = 5
            t_side = 4
    else:
        if side == 1:
            f_side = 0
            s_side = 3
            t_side = 5
        elif side == 2:
            f_side = 0
            s_side = 4
            t_side = 5
        elif side == 3:
            f_side = 0
            s_side = 1
            t_side = 5
        elif side == 4:
            f_side = 0
            s_side = 2
            t_side = 5
        elif side == 5:
            f_side = 3
            s_side = 0
            t_side = 1
        elif side == 0:
            f_side = 1
            s_side = 5
            t_side = 3
    return f_side, s_side, t_side 
                  
    
def turne(side, segment, turne_number=1):
    '''
    side:         positive int from 1 to 6, wich describe side of cube
    segment:      positive int, describe axis for rotation; 1 to 3 - horizontal, 4 - 6 vertical
    turne_number: positive int, describe number of turns
    
    return:       cube with roteded sides
    
    assumption that cube can turn only by clockwise or down
    
    '''
    global cube
    cube = cube
    if side < 1 or segment < 1 or turne_number < 1 or side > 6 or segment > 6:
        print'The turne function has stoped! Incorrect arguments'
        return
    
    turne_number = turne_number % 4
    if turne_number == 0:
        return
        
    side -= 1
    segment -= 1
    f_side, s_side, t_side = other_side_calculation(side, segment)
    
    if  segment  < 3:       
        for i in xrange(turne_number):
            temp = cube[side][segment]
            cube[side][segment] = cube[f_side][segment]
            cube[f_side][segment] = cube[s_side][segment]
            cube[s_side][segment] = cube[t_side][segment]
            cube[t_side][segment] = temp
    else:
        segment = segment - 3
        for i in xrange(turne_number):
            temp = [cube[side][0][segment], cube[side][1][segment], cube[side][2][segment]]
            cube[side][0][segment] = cube[f_side][0][segment]
            cube[side][1][segment] = cube[f_side][1][segment]
            cube[side][2][segment] = cube[f_side][2][segment]
            
            cube[f_side][0][segment] = cube[s_side][0][segment]
            cube[f_side][1][segment] = cube[s_side][1][segment]
            cube[f_side][2][segment] = cube[s_side][2][segment]
            
            cube[s_side][0][segment] = cube[t_side][0][segment]
            cube[s_side][1][segment] = cube[t_side][1][segment]
            cube[s_side][2][segment] = cube[t_side][2][segment]

            cube[t_side][0][segment] = temp[0]
            cube[t_side][1][segment] = temp[1]
            cube[t_side][2][segment] = temp[2]
     
def turn_test():
    from copy import deepcopy
    global cube
    cube_before = deepcopy(cube)
    print "*********1*************"
    side = 1
    sgment = 1
    turne_number = 0
    turne(side, sgment, turne_number)
    if turne_number == 0:
        if cube_before ==  cube:
            print 'turne_number == 0 -> ok'
        else:
            print 'turne_number == 0 -> error'

    print "*********2*************"
    side = 1
    sgment = 1
    turne_number = 4
    turne(side, sgment, turne_number)
    if turne_number == 4:
        if cube_before ==  cube:
            print 'turne_number % 4 == 0 -> ok'
        else:
            print 'turne_number % 4 == 0 -> error'
        
    print "*********3*************"
    side = 1
    segment = 0
    turne_number = 1
    turne(side, segment, turne_number)
    if side < 1 or segment < 1 or turne_number < 1 or side > 6 or segment > 6:
        if cube_before ==  cube:
            print 'incorrect data -> ok'
        else: 
            print 'incorrect data -> error'
    print "*********4*************"
    side = 0
    segment = 1
    turne_number = 1
    turne(side, segment, turne_number)
    if side < 1 or segment < 1 or turne_number < 1 or side > 6 or segment > 6:
        if cube_before ==  cube:
            print 'incorrect data -> ok'
            
        else: 
            print 'incorrect data -> error'
        
    print "*********5*************"
    side = 7
    sgment = 1
    turne_number = 1
    turne(side, segment, turne_number)
    if side < 1 or segment < 1 or turne_number < 1 or side > 6 or segment > 6:
        if cube_before ==  cube:
            print 'incorrect data -> ok'
            
        else: 
            print 'incorrect data -> error'
    print "*********6*************"
    cube_before = deepcopy(cube)
    side = 1
    segment = 1
    turne_number = 1
    turne(side, segment, turne_number)
    seg = segment
    segment = segment - 1
    if cube[0][segment] == cube_before[2][segment] and cube[4][segment] == cube_before[0][segment] and cube[5][segment] == cube_before[4][segment] and cube[2][segment] == cube_before[5][segment] and cube[1][segment] == cube_before[1][segment] and cube[3][segment] == cube_before[3][segment]:
        print 'side = {}, segment = {}, turne_number = {} -> ok'. format(side, seg, turne_number)
    else:
        print 'side = {}, segment = {}, turne_number = {} -> error'. format(side, seg, turne_number)
    print "*********7*************"
    cube_before = deepcopy(cube)
    side = 2
    segment = 1
    turne_number = 5
    turne(side, segment, turne_number)
    seg = segment
    segment = segment - 1
    if cube[1][segment] == cube_before[4][segment] and cube[2][segment] == cube_before[1][segment] and cube[3][segment] == cube_before[2][segment] and cube[4][segment] == cube_before[3][segment] and cube[0][segment] == cube_before[0][segment] and cube[5][segment] == cube_before[5][segment]:
        print 'side = {}, segment = {}, turne_number = {} -> ok'. format(side, seg, turne_number)
    else:
        print 'side = {}, segment = {}, turne_number = {} -> error'. format(side, seg, turne_number)
    print "*********8*************"
    cube_before = deepcopy(cube)
    side = 6
    segment = 1
    turne_number = 5
    turne(side, segment, turne_number)
    seg = segment
    segment = segment - 1
    if cube[5][segment] == cube_before[2][segment] and cube[2][segment] == cube_before[0][segment] and cube[0][segment] == cube_before[4][segment] and cube[4][segment] == cube_before[5][segment] and cube[3][segment] == cube_before[3][segment] and cube[1][segment] == cube_before[1][segment]:
        print 'side = {}, segment = {}, turne_number = {} -> ok'. format(side, seg, turne_number)
    else:
        print 'side = {}, segment = {}, turne_number = {} -> error'. format(side, seg, turne_number)

    print "*********9*************"
    cube_before = deepcopy(cube)
    side = 1
    segment = 2
    turne_number = 1
    turne(side, segment, turne_number)
    seg = segment
    segment = segment - 1
    if cube[0][segment] == cube_before[2][segment] and cube[4][segment] == cube_before[0][segment] and cube[5][segment] == cube_before[4][segment] and cube[2][segment] == cube_before[5][segment] and cube[1][segment] == cube_before[1][segment] and cube[3][segment] == cube_before[3][segment]:
        print 'side = {}, segment = {}, turne_number = {} -> ok'. format(side, seg, turne_number)
    else:
        print 'side = {}, segment = {}, turne_number = {} -> error'. format(side, seg, turne_number)
    
    print "*********10*************"
    cube_before = deepcopy(cube)
    side = 1
    segment = 3
    turne_number = 1
    turne(side, segment, turne_number)
    seg = segment
    segment = segment - 1
    if cube[0][segment] == cube_before[2][segment] and cube[4][segment] == cube_before[0][segment] and cube[5][segment] == cube_before[4][segment] and cube[2][segment] == cube_before[5][segment] and cube[1][segment] == cube_before[1][segment] and cube[3][segment] == cube_before[3][segment]:
        print 'side = {}, segment = {}, turne_number = {} -> ok'. format(side, seg, turne_number)
    else:
        print 'side = {}, segment = {}, turne_number = {} -> error'. format(side, seg, turne_number)
        
    print "*********11*************"
    cube_before = deepcopy(cube)
    side = 2
    segment = 2
    turne_number = 5
    turne(side, segment, turne_number)
    seg = segment
    segment = segment - 1
    if cube[1][segment] == cube_before[4][segment] and cube[2][segment] == cube_before[1][segment] and cube[3][segment] == cube_before[2][segment] and cube[4][segment] == cube_before[3][segment] and cube[0][segment] == cube_before[0][segment] and cube[5][segment] == cube_before[5][segment]:
        print 'side = {}, segment = {}, turne_number = {} -> ok'. format(side, seg, turne_number)
    else:
        print 'side = {}, segment = {}, turne_number = {} -> error'. format(side, seg, turne_number)
    

    print "*********12*************"
    cube_before = deepcopy(cube)
    side = 6
    segment = 2
    turne_number = 5
    turne(side, segment, turne_number)
    seg = segment
    segment = segment - 1
    if cube[5][segment] == cube_before[2][segment] and cube[2][segment] == cube_before[0][segment] and cube[0][segment] == cube_before[4][segment] and cube[4][segment] == cube_before[5][segment] and cube[3][segment] == cube_before[3][segment] and cube[1][segment] == cube_before[1][segment]:
        print 'side = {}, segment = {}, turne_number = {} -> ok'. format(side, seg, turne_number)
    else:
        print 'side = {}, segment = {}, turne_number = {} -> error'. format(side, seg, turne_number)
    
    print "*********13*************"
    cube = make_cube()
    cube_before = deepcopy(cube)
    side = 1
    segment = 4
    turne_number = 5
    turne(side, segment, turne_number)
    side_p = side
    side = side - 1
    f_side, s_side, t_side = other_side_calculation(side, segment)
    s = segment - 4
    if cube[side][0][s] == cube_before[f_side][0][s] and \
       cube[side][1][s] == cube_before[f_side][1][s] and \
       cube[side][2][s] == cube_before[f_side][2][s] and \
       cube[f_side][0][s] == cube_before[s_side][0][s] and \
       cube[f_side][1][s] == cube_before[s_side][1][s] and \
       cube[f_side][2][s] == cube_before[s_side][2][s] and  \
       cube[s_side][0][s] == cube_before[t_side][0][s] and  \
       cube[s_side][1][s] == cube_before[t_side][1][s] and \
       cube[s_side][2][s] == cube_before[t_side][2][s] and \
       cube[t_side][0][s] == cube_before[side][0][s] and \
       cube[t_side][1][s] == cube_before[side][1][s] and \
       cube[t_side][2][s] == cube_before[side][2][s]:
       print 'side = {}, segment = {}, turne_number = {} -> ok'. format(side_p, segment, turne_number)
    else:
        print 'side = {}, segment = {}, turne_number = {} -> error'. format(side_p, segment, turne_number)

    print "*********14*************"
    cube = make_cube()
    cube_before = deepcopy(cube)
    side = 1
    segment = 5
    turne_number = 5
    turne(side, segment, turne_number)
    side_p = side
    side = side - 1
    f_side, s_side, t_side = other_side_calculation(side, segment)
    s = segment - 4
    if cube[side][0][s] == cube_before[f_side][0][s] and \
       cube[side][1][s] == cube_before[f_side][1][s] and \
       cube[side][2][s] == cube_before[f_side][2][s] and \
       cube[f_side][0][s] == cube_before[s_side][0][s] and \
       cube[f_side][1][s] == cube_before[s_side][1][s] and \
       cube[f_side][2][s] == cube_before[s_side][2][s] and  \
       cube[s_side][0][s] == cube_before[t_side][0][s] and  \
       cube[s_side][1][s] == cube_before[t_side][1][s] and \
       cube[s_side][2][s] == cube_before[t_side][2][s] and \
       cube[t_side][0][s] == cube_before[side][0][s] and \
       cube[t_side][1][s] == cube_before[side][1][s] and \
       cube[t_side][2][s] == cube_before[side][2][s]:
       print 'side = {}, segment = {}, turne_number = {} -> ok'. format(side_p, segment, turne_number)
    else:
        print 'side = {}, segment = {}, turne_number = {} -> error'. format(side_p, segment, turne_number)

        
if __name__ == '__main__':    
    print 'Size of sides = ', x
    cube = make_cube()
    show_cube()
    turn_test()
